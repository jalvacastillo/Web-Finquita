<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $app->get('/', function () use ($app) {return $app->version(); });

    // $app->get('/key', function() {return str_random(32); });

    Route::get('/', function(){ 
        return view('welcome');
    });
    
    Route::get('correo', function(){

        $cliente['nombre'] = "Jesus Alvarado";
        $cliente['correo'] = "alvarado.websis@gmail.com";
        $cliente['mensaje'] = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore debitis quo consectetur necessitatibus porro consequuntur, ratione eius odit magni ut hic, quaerat aut expedita, blanditiis laudantium nam aliquam quos repellendus.";

        return view('emails.contacto', compact('cliente'));
    });

    Route::post('correo', 'HomeController@correo');
