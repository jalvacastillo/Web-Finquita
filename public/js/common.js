jQuery(function ($) {

    'use strict';
	
// =============================================
// BEGIN THEME SCRIPTS
// =============================================


// Script Wow Animation
new WOW().init();

//Parallax	
jQuery(window).bind('load', function () {
	parallaxInit();						  
});

function parallaxInit() {
    jQuery('.parallax').each(function(){
        jQuery(this).parallax("30%", 0.1);
    });
}



// Somth page scroll
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top -40
        }, 1000);
        return false;
      }
    }
  });
});

$(document).ready(function() {
    $("#lightgallery").lightGallery({thumbnail:true}); 
});

// =============================================
// END THEME SCRIPTS
// =============================================
	
});

angular.module('app', [])

.constant("config", {
    "url": "http://www.finquitatresmarias.com/"
})

.controller('EmailCtrl', ['$scope', '$http', 'config', function ($scope, $http, config) {
    $scope.email = function(correo){
        console.log(correo);
        if($scope.correo){
            $scope.loader = true;
            $http.post(config.url + 'correo', $scope.correo).
              success(function(data, status) {
                if (status == 200) {
                    $scope.correo = {};
                    $scope.loader = false;
                    $scope.emailErrores = "Mensaje Enviado: Gracias por escribirnos";
                }else{
                    $scope.loader = false;
                    $scope.emailErrores = data;
                }
              }).
              error(function(data, status) {
                $scope.loader = false;
                $scope.emailErrores = "El mensaje no pudo ser enviado, revice su conexión a internet.";
              });
        }
    }
}]);