<section class="aboutus" id="services">
<div class="container">  
    <div class="heading text-center">
        <img class="dividerline" src="img/sep.png" alt="">
        <h2>Servicio</h2>
        <span>Disfruta de nuestro alojamiento y gastronomía</span>
        <img class="dividerline" src="img/sep.png" alt="">
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="papers text-center">
                <img src="/img/hotel.jpg" alt=""><br/>
                <a href="#"><b>Hotel</b></a>
                <h4 class="notopmarg nobotmarg">4 bellas habitaciones</h4>
                <p>
                    En La “Finquita las tres Marías” el alojamiento es de primera, contamos con 4 bellas habitaciones dobles.
                    <br><br>
                    Sus habitaciones son confortables y todas están rodeadas y adornadas de la flora que caracteriza al lugar, son un excelente lugar para descansar, relajarse y disfrutar de todas nuestras instalaciones.
                    <br><br>
                    Todas las habitaciones cuentas con:
                <ul style="text-align: left;">
                    <li> Aire acondicionado.</li>
                    <li> TV por cable.</li>
                    <li> Agua caliente y fría.</li>
                    <li> Amplio baño con closet.</li>
                    <li> Derecho a uso de piscina, bar y restaurante.</li>
                </ul>
                </p>
                <a href="tel:+503 7894 8672" class="btn btn-danger">
                    <i class="fa fa-whatsapp"></i> Llamar
                </a>
                <a href="#contact" class="btn btn-danger">
                    <i class="fa fa-check"></i> Reservar
                </a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="papers text-center">
                <img src="/img/restaurante.jpg" alt=""><br/>
                <a href="#"><b>Restaurante</b></a>
                <h4 class="notopmarg nobotmarg">Platillos exquisitos</h4>
                <p>
                    El Restaurante te ofrece un amplio menú en alimentos y bebidas, ofreciendo <strong>desayunos, almuerzos y cenas</strong>, el lugar cuenta con espacios al aire libre ambientada con la naturaleza y con zonas privadas para que disfrutes de espacio más reservado.
                    <br><br>
                    El menú o carta del restaurante es fundamental también se puede decir que este uno de los componentes que caracteriza al restaurante la carta está dividida en los siguientes componentes:

                    <ul style="text-align: left;">
                        <li>Entradas</li>
                        <li>Ensaladas</li>
                        <li>Platos fuertes</li>
                        <li>Postres</li>
                        <li>Bebidas</li>
                    </ul>
                </p>
                 <a href="tel:+503 7894 8672" class="btn btn-danger">
                     <i class="fa fa-whatsapp"></i> Llamar
                 </a>
                 <a href="#contact" class="btn btn-danger">
                     <i class="fa fa-check"></i> Reservar
                 </a>
            </div>
        </div>
    </div>
</div>
</section>