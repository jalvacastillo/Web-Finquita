<section class="aboutus" id="aboutus">
<div class="container">
    <div class="heading text-center">
        <img class="dividerline" src="img/sep.png" alt="">
        <h2>Sobre Nosotros</h2>
        <img class="dividerline" src="img/sep.png" alt="">
        <h3>
            Desde el 2006 la Finquita "Las Tres Marías" se convirtió en el primer lugar en el departamento de Cabañas, que en medio de su ambiente fresco y natural se ha convertido en el preferido por su deliciosa comida y su atención de primera.
            <br><br>
            Contamos con 4 Cabañas para alojamiento, piscina, restaurante y un salón de usos múltiples y espacio para cualquier tipo de evento empresarial, social o familiar.
        </h3>
    </div>    
</div>
</section>