<!DOCTYPE html>
<html>
    <head>
        <title>Websis - 405</title>
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row text-center">
                <br><br>
                <img src="/img/logotipo.svg" alt="Page not found" width="400">
                <hr>

                <h2 class="headline text-warning"> 405</h2>
                <h3> <i class="fa fa-warning text-warning"></i> Oops! Algo anda mal. </h3>
                <p> Nosotros trabajaremos para que vulva a funcionar. 
                Quieres regresar <a href="javascript:history.back();">atrás. </a>
                </p>
                <br>
                <form class="search-form">
                <div class="input-group">
                  <input type="text" name="search" class="form-control" placeholder="Buscar">
                  <div class="input-group-btn">
                    <button type="submit" name="submit" class="btn btn-danger btn-flat"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
                <br>
            </div>
        </div>
    </body>
</html>
