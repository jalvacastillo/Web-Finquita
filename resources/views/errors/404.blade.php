<!DOCTYPE html>
<html>
    <head>
        <title>Finquita - 404</title>
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row text-center">
                <br><br>
                <img src="/img/logo.png" alt="Page not found" width="400">
                <hr>

                <h2 class="headline text-warning"> 404</h2>
                <h3> <i class="fa fa-warning text-warning"></i> Oops! Página no encontrada. </h3>
                <p> No podimos encontrar la página que buscabas. 
                Quieres regresar <a href="javascript:history.back();">atrás. </a>
                </p>
                <br>
            </div>
        </div>
    </body>
</html>
