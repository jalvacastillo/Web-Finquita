<header>
    <div class="banner row" id="banner">        
        <div class="parallax text-center" style="background-image: url(/img/banner.jpg);">
            <div class="parallax-pattern-overlay">
                <div class="container text-center" style="height:580px;padding-top:170px;">
                    <a href="#"><img id="site-title" class=" wow fadeInDown" wow-data-delay="0.0s" wow-data-duration="0.9s" src="img/logo.png" alt="logo"/></a>
                    <h2 class="intro wow zoomIn" wow-data-delay="0.4s" wow-data-duration="0.9s">Hotel & Restaurante</h2>
                    <p style="border-radius: 10px; color: white; padding: 10px; background: rgba(0,0,0,.5); width: 200px; margin: auto;" class="wow fadeInUp" wow-data-delay="3000s" wow-data-duration="3000s">
                        Celebrando 10 años
                    </p>
                    <br>
                    <a href="tel:+503 7894 8672" style="width:200px;" class="btn btn-success">
                    <i class="fa fa-whatsapp"></i> Llamar
                    </a>
                    <a href="javascript: void(0);" style="width:200px;" class="btn btn-primary" onclick="window.open('http://www.facebook.com/sharer.php?u=finquitatresmarias.com','popup', 'toolbar=0, status=0, width=650, height=450');">
                    <i class="fa fa-facebook-official"></i> Compartir
                    </a>
                </div>
            </div>
        </div>
    </div>  
    <div class="menu">
        <div class="navbar-wrapper">
            <div class="container">
                <div class="navwrapper">
                    <div class="navbar navbar-inverse navbar-static-top">
                        <div class="container">
                            <div class="navArea">
                                <div class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav">
                            {{-- <li class="menuItem active"><a href="#wrapper">Inicio</a></li> --}}
                            <li class="menuItem"><a href="#aboutus">Nosotros</a></li>
                            <li class="menuItem"><a href="#services">Servicios</a></li>
                            {{-- <li class="menuItem"><a href="#eventos">Eventos</a></li> --}}
                            <li class="menuItem"><a href="#gallery">Galeria</a></li>
                            <li class="menuItem"><a href="#feedback">Clientes</a></li>
                            <li class="menuItem"><a href="#contact">Contactanos</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
    </header>