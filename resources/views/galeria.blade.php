<section class="gallery" id="gallery">
    <div class="container">
        <div class="heading text-center">
            <img class="dividerline" src="img/sep.png" alt="">
            <h2>Nuestra Galeria</h2>
            <span>Descubre nuestras amplias instalaciones</span>
            <img class="dividerline" src="img/sep.png" alt="">
        </div>
        
        <div id="grid-gallery3" class="grid-gallery">
              <div id="lightgallery">
                  <?php
                      $ruta = "img/instalaciones";
                      $filehandle = opendir($ruta);
                        while ($file = readdir($filehandle)) {
                              if ($file != "." && $file != "..") {
                                  echo '<a href="'.$ruta."/".$file.'" class="col-md-4 col-xs-6" style="overflow: hidden; height: 300px; padding: 2px 4px;"> <img src="'.$ruta."/".$file.'" style="height: 100%;"/> </a>';
                              } 
                        } 
                      closedir($filehandle);
                  ?>
              </div>
            </div><!-- // grid-gallery -->
        </div>
</section>