<section class="feedback" id="feedback">
<div class="container w960">
    <div class="heading">
        <img class="dividerline" src="img/sep.png" alt="">
        <h2>Nuestros clientes</h2>
        <img class="dividerline" src="img/sep.png" alt="">
        <h3>Nos gusta consentir a nuestros clientes, y que nos den su opinión</h3>
    </div>
    <div class="row">
    <div class="col-xs-6">
        <blockquote>
            Una experiencia única, espectacular, lugar tranquilo, se respira aire fresco, puro, paz, la atención buenísima. ¡La comida espectacular, Dios los bendiga Bendiciones!
            <cite>
                Ivelisse Romero<br/><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
            </cite> 
        </blockquote>
    </div>
    <div class="col-xs-6">
        <blockquote>
            ¡Todas las trabajadoras de la Finquita son excelentes! El servicio es súper, la comida muy rica y el lugar espectacular. Regresaremos el otro año para ir a relajarnos en nuestras vacaciones. ¡Saludos a Marisela que nos trató muy bien!
            <cite>Eloisa Castillo<br/><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
            </cite> 
        </blockquote>
    </div>
    <div class="col-xs-6">
        <blockquote>
            ¡Excelente lugar mis hijos encantados cuando han ido!
            <cite>Lily Rivera<br/><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
            </cite>
        </blockquote>
    </div>
    <div class="col-xs-6">
        <blockquote>
            Es un lugar espectacular con una vegetación hermosa, un lugar para relajarse en pareja y en familia le cubre todas las expectativas. Visítelo le va a encantar.
            <cite>Familia Mendez Larreynaga<br/><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
            </cite>
        </blockquote>
    </div>
    </div>
</div>
</section>