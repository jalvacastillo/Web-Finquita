<section class="footer" id="footer">
<p class="text-center">
    <a href="#wrapper" class="gototop"><i class="fa fa-angle-double-up fa-2x"></i></a>
</p>
<div class="container">
    <ul class="row">
        <a href="tel:503 23890541" class="col-md-4">
            <li >
                <i class="fa fa-phone"></i><br>+(503) 2389-0541
            </li>
        </a>
        <a href="https://www.facebook.com/LasTresMariasFinquita/" target="_blank" class="col-md-4">
            <li>
                <i class="fa fa-facebook"></i><br>LasTresMariasFinquita
            </li>
        </a>
        <a href="mailto:finquita3marias@hotmail.com" class="col-md-4">
            <li>
                <i class="fa fa-send"></i><br>finquita3marias@hotmail.com
            </li>
        </a>
    </ul>
    <p>
        &copy; 2016 Finquita Tres Marias<br>
         by <a href="http://cri.catolica.edu.sv/cdmype" target="_blank">CDMYPE Ilobasco</a>
    </p>
</div>
</section>