<!DOCTYPE html>
<html class="no-js" lang="en">
<head>

<meta charset="UTF-8"/>

<title>Finquita – Las Tres Marías</title>
<meta name="description" content="Es un lugar espectacular con una vegetación hermosa, un lugar para relajarse en pareja y en familia le cubre todas las expectativas. Visitelo le va a encantar.">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
<link rel="stylesheet" href="css/font-awesome.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="{{ asset('lightGallery/css/lightgallery.min.css') }}">
<link rel="stylesheet" href="css/theme.css">

<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>

</head>
<body>

<div class="wrapper" id="wrapper">
    
    @include('header')
    @include('nosotros')
    @include('servicios')
    @include('otrosservicios')
    @include('galeria')
    @include('clientes')
    @include('contactos')
    @include('footer')
    
</div>

<!--Javascripts-->
<script src="js/jquery.js"></script>
<script src="js/modernizr.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/menustick.js"></script>
<script src="js/parallax.js"></script>
{{-- <script src="js/easing.js"></script> --}}
<script src="js/wow.js"></script>
<!-- Google Map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
<script src="js/google_map.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="{{ asset('lightGallery/js/lightgallery.min.js') }}"></script>
<script src="{{ asset('lightGallery/js/lg-thumbnail.min.js') }}"></script>
<script src="{{ asset('lightGallery/js/lg-fullscreen.min.js') }}"></script>
<script src="js/classie.js"></script>
<script src="js/common.js"></script>
{{-- <script src="js/masonry.js"></script> --}}
{{-- <script src="js/imgloaded.js"></script> --}}
{{-- <script src="js/colorfinder.js"></script> --}}
{{-- <script src="js/gridscroll.js"></script> --}}
{{-- <script src="js/contact.js"></script> --}}

<script type="text/javascript">
jQuery(function($) {
$(document).ready( function() {
  //enabling stickUp on the '.navbar-wrapper' class
    $('.navbar-wrapper').stickUp({
        parts: {
          // 0: 'banner',
          0: 'aboutus',
          1: 'services',
          // 2: 'eventos',
          2: 'gallery',
          // 5: 'specialties',
          3: 'feedback',
          4: 'contact'
        },
        itemClass: 'menuItem',
        itemHover: 'active',
        topMargin: 'auto'
        });
    });
});
</script>
</body>
</html>