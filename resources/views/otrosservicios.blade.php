<section class="gallery" id="eventos">
    <div class="container">
        <div class="heading text-center">
            <img class="dividerline" src="img/sep.png" alt="">
            <h2>Otros servicios</h2>
            <span>Te ofrecemos lo que necesites para tus eventos y reuniones familiares</span>
            <img class="dividerline" src="img/sep.png" alt="">
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="papers text-center">
                    <img src="/img/salon.jpg" alt=""/>
                    <h3>Sala de conferencias</h3>
                    <p>Contamos con un salón exclusivo para realizar cualquier tipo de evento empresarial como: <strong>Capacitaciones, Conferencias, Reuniones de negocio, Y más.</strong>
                    <br><br>
                    El salón cuenta con:
                       <ul style="text-align: left;">
                            <li>Capacidad para 250 personas</li>
                            <li>Mesas y sillas</li>
                            <li>Sonido</li>
                            <li>Proyector y pizarra</li>
                            <li>Aire acondicionado</li>
                            <li>Servicio de comida y bebida</li>
                        </ul>
                    </p>
                    <a href="tel:+503 7894 8672" class="btn btn-danger">
                        <i class="fa fa-whatsapp"></i> Llamar
                    </a>
                    <a href="#contact" class="btn btn-danger">
                        <i class="fa fa-check"></i> Reservar
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="papers text-center">
                    <img src="/img/piscina.jpg" alt=""/>
                    <h3>Piscina</h3>
                    <p>Uno de nuestros atractivos es la Piscina, especial para que nuestros clientes disfruten y se refresquen en un lugar adornado por la naturaleza.
                    <br><br>
                    La piscina cuenta con:
                        <ul style="text-align: left;">
                            <li>20 metros de largo</li>
                            <li>2.5 de profundidad</li>
                            <li>Área para niños</li>
                            <li>Área para niños</li>
                            <li>Área para niños</li>
                        </ul>
                    </p>
                    <a href="tel:+503 7894 8672" class="btn btn-danger">
                        <i class="fa fa-whatsapp"></i> Llamar
                    </a>
                    <a href="#contact" class="btn btn-danger">
                        <i class="fa fa-check"></i> Reservar
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="papers text-center">
                    <img src="/img/eventos.jpg" alt=""/>
                    <h3>Eventos</h3>
                    <p>Nuestras instalaciones son muy amplias y apropiadas para realizar cualquier tipo de evento social como: <strong>Bodas, Fiestas rosas, Celebraciones especiales y más.</strong>
                    <br><br>
                    Para eventos ofrecemos:
                        <ul style="text-align: left;">
                            <li>Estacionamiento</li>
                            <li>Servicio de comida y bebida</li>
                            <li>Uso de piscina</li>
                            <li>Uso de zona verde.</li>
                            <li>Meseros</li>
                            <li>Arreglo del local</li>
                        </ul>
                    </p>
                    <a href="tel:+503 7894 8672" class="btn btn-danger">
                        <i class="fa fa-whatsapp"></i> Llamar
                    </a>
                    <a href="#contact" class="btn btn-danger">
                        <i class="fa fa-check"></i> Reservar
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>