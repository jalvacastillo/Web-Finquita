<section class="contact" id="contact" ng-app="app">
<div class="container">
    <div class="heading">
        <img class="dividerline" src="img/sep.png" alt="">
        <h2>Comunicate con nosotros</h2>
        <img class="dividerline" src="img/sep.png" alt="">
        <h3>
            Escríbenos o llámanos, te atenderemos lo más pronto posible
        </h3>
    </div>
</div>
 <div class="container w960">
  <div class="row">
  <div class="col-md-6" ng-controller="EmailCtrl">
  
    <div ng-show="emailErrores">
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            Tu mensaje ha sido enviado. ¡Gracias!
        </div>
    </div>
   <form ng-submit="email(correo);" id="contactform">
      <input ng-model="correo.nombre" type="text" class="contact col-xs-12" placeholder="Nombre" required>
      <input ng-model="correo.email" type="email" class="contact noMarr col-xs-12" placeholder="Tu correo" required>
      <textarea ng-model="correo.mensaje" class="contact col-xs-12" placeholder="Mensaje" required></textarea>
      <input type="submit" id="submit" class="contact submit" value="Enviar">
    </form>
      <br>
  </div>
  <div class="col-md-6">
    <div id="map" style="width:100%; height:300px;"> </div>
  </div>
</div>
</div>
</section>