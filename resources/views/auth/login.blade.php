@extends('pagina.base')

@section('content')
<div class="login" class="container">

    <div class="row animated flipInY">
        <div class="col-xs-0 col-sm-3">
            <!-- Izquierda -->
        </div>
        
        <div class="col-xs-12 col-sm-6">
            
        <!-- Emcabezado -->
            <center>
                <img class='img-responsive' src="/img/logotipo.svg">
            </center>
            <legend></legend>
    
                
        <!-- Formulario -->
         <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
            {!! csrf_field() !!}
            
            <!-- Panel -->
            <div class="panel panel-primary">

                <div class="panel-heading text-center">
                    <h2 class="panel-title">Bienvenido</h2>
                </div>

                <div class="panel-body">
                    <div class="col-xs-12">
                    <!-- Correo -->  
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label class="control-label">Correo Electrónico</label>
                        <div class="input-group">
                            <div class="input-group-addon glyphicon glyphicon-envelope"></div>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                        </div>
                    </div>
                        
                    <!-- Contraseña -->
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label class="control-label">Password</label>
                        <div class="input-group">
                            <div class="input-group-addon glyphicon glyphicon-lock"></div>
                            <input type="password" class="form-control" name="password">
                        </div>
                    </div>

                    {{-- Remember --}}
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember"> Recordarme
                            </label>
                        </div>
                    </div>
                    
                    {{-- Forgot --}}
                    <div class="form-group">
                        <a class="btn btn-link" href="{{ url('/password/reset') }}">
                            Olvidaste tu contraseña?
                        </a>
                    </div>
                    </div>
                </div>

                <div class="panel-footer">
                    <button type="submit" tabindex="3" class="btn btn-primary btn-lg btn-block">
                        <span class="glyphicon glyphicon-ok"></span>
                         Iniciar sesión
                    </button>
                </div>
            
            </div>
        </form>
        
        <div class="col-xs-1 col-sm-3">
            <!-- derecha -->
        </div>
    </div>

</div>
@endsection