@extends('pagina.base')

@section('content')

<div class="login" class="container">

    <div class="row animated flipInY">
        <div class="col-xs-0 col-sm-4">
            <!-- Izquierda -->
        </div>
        
        <div class="col-xs-12 col-sm-4">
            
        <!-- Emcabezado -->
        <center>
            <img class='img-responsive' src="/img/logotipo.svg">
        </center>
        <legend></legend>
                
        <!-- Formulario -->
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
        {!! csrf_field() !!}

        <div class="panel panel-primary">

            <div class="panel-heading text-center">
                <h2 class="panel-title">Restablecer acceso</h2>
            </div>

            <div class="panel-body">
                <div class="col-xs-12">

                 @if (session('status'))
                     <div class="alert alert-success">
                         {{ session('status') }}
                     </div>
                 @endif

                 <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                     <label class="col-md-4 control-label">Correo Electrónico</label>
                     <div class="col-md-6">
                         <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                     </div>
                 </div>
            </div>

            <div class="panel-footer">
                <button type="submit" tabindex="3" class="btn btn-primary btn-lg btn-block">
                    <i class="fa fa-btn fa-envelope"></i> Resetear contraseña
                </button>
            </div>
        </div>
        
        </form>         
        
        <div class="col-xs-1 col-sm-4">
            <!-- derecha -->
        </div>
    </div>

</div>
@endsection

